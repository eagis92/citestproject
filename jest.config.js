module.exports = {
    "roots": [
        "<rootDir>/src"
    ],
    "testMatch": ["**/*.[jt]s?(x)", "**/?(*.)+(spec|test).[tj]s?(x)"],
    "moduleFileExtensions": [
        "js",
        "jsx",
    ],
};