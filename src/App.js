import React, {Component} from 'react';

import './App.scss';
import {ComboBox} from "./ComboBox/ComboBox";
import {Chip} from "./Chip/Chip";

const settings = {
    id: "1d926cc1-68ca-45cf-9e8a-6e9da419b709",
    isOpened: false,
    multiple: false,
    title: "Combo Single Selection",
    icon: "arrow-down-12",
    iconPosition: "right",
    options: [
        {
            title: "24 Hours",
            value: "1",
            style: "someClassName"
        },
        {
            title: "One Week",
            value: "7",
        },
        {
            title: "This Month",
            value: "31",
        },
        {
            title: "This Year",
            value: "365",
        }
    ]
};
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: settings.options[0]
        };
        this.onSelect = this.onSelect.bind(this);
        this.onRemove = this.onRemove.bind(this);
    }

    onSelect({selectedOption}) {
        this.setState({selectedOption});
    }

    onRemove() {
        console.log("chip on remove");
    }

    render() {
        const chipSettings = {
            label: "Last 24 hours",
            title: "Date/Time",
            labelStyle: "custom-style-name",
            value: 0
        };
        return (
            <div className="App">
                <ComboBox
                    settings={settings}
                    onSelect={this.onSelect}
                    selectedOption={this.state.selectedOption}
                />
                <p>{this.state.selectedOption.title}</p>
                <Chip {...chipSettings} onRemove={this.onRemove}/>
            </div>
        );
    }
}

export default App;