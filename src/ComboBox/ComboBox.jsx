/**
 *      *** Combo Button Component ***
 * */

/* settings props must have a structure like this
const settings = {
    id: "1d926cc1-68ca-45cf-9e8a-6e9da419b709",
    isOpened: false,
    multiple: false,
    title: "Combo Single Selection",
    icon: "arrow-down-12",
    iconPosition: "right",
    options: [
        {
            label: "24 Hours",
            value: "1",
            count: 2,
            style: "someClassName"
        },
        {
            label: "One Week",
            value: "7",
            count: 5,
        },
        {
            label: "This Month",
            value: "31",
            count: 8,
        },
        {
            label: "This Year",
            value: "365",
            count: 14,
        }
    ],
    customOptions: [
        {
            label: "Custom Date/Time",
            value: "111",
            count: 111
        },
        {
            label: "Custom Date/Time2",
            value: "222",
            count: 222
        }
    ]
};
*/

import React, {Component} from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import ComboBoxOptions from "./ComboBoxOptions";
import ComboBoxButton from "./ComboBoxButton";
import {createDisplayableText, getComboOptionPosition} from "../helpers";

class ComboBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpened: this.props.settings.isOpened,
            optionsListPosition: {}
        };

        this.onOpen = this.onOpen.bind(this);
        this.onClose = this.onClose.bind(this);
        this.onSelect = this.onSelect.bind(this);
        this.onOpenClose = this.onOpenClose.bind(this);
        this.onOptionToggle = this.onOptionToggle.bind(this);
        this.onOpenCallBack = this.onOpenCallBack.bind(this);
        this.onCloseCallBack = this.onCloseCallBack.bind(this);
        this.onOpenCloseCallBack = this.onOpenCloseCallBack.bind(this);
        this.onCustomOptionSelect = this.onCustomOptionSelect.bind(this);
        this.processSingleSelection = this.processSingleSelection.bind(this);

        this.buttonRef = React.createRef();
    }

    onOpenClose(updatedState) {
        const {optionsPortalPosition} = this.props;
        return (event) => {
            const optionsListPosition = getComboOptionPosition(
                updatedState, event, optionsPortalPosition
            );
            this.setState({
                isOpened: updatedState,
                optionsListPosition
            }, this.onOpenCloseCallBack);
        };
    }

    onOpenCloseCallBack() {
        const {isOpened} = this.state;

        if (isOpened) {
            this.onOpenCallBack();
        } else {
            this.onCloseCallBack();
        }
    }

    onOpen() {
        this.setState({
            isOpened: true
        });
    }

    onClose() {
        this.setState({
            isOpened: false
        });
    }

    onOpenCallBack() {
        const {state, props} = this;
        const toParentPayload = {
            state,
            props,
            onClose: this.onClose,
            onOpen: this.onOpen
        };

        this.props.onOpen(toParentPayload);
    }

    onCloseCallBack() {
        const {state, props} = this;
        const toParentPayload = {
            state,
            props,
            onClose: this.onClose,
            onOpen: this.onOpen
        };

        this.props.onClose(toParentPayload);
    }

    onSelect(selectedOption) {
        const validationOutput = this.props.validateSelection(selectedOption);
        if (validationOutput && validationOutput.error) {
            selectedOption["validation"] = validationOutput;
            this.props.onInvalidSelection(selectedOption);
        } else {
            this.onOptionToggle(selectedOption);
        }
    }

    processMultiSelection({value, title}, itemsSelectionState) {
        let toParentPayload = {
            onClose: this.onClose,
            onOpen: this.onOpen
        };
        let upToDateState = itemsSelectionState || {};
        if (itemsSelectionState.hasOwnProperty(value)) {
            upToDateState = {
                ...itemsSelectionState,
                [value]: {
                    selected: !itemsSelectionState[value].selected,
                }
            };
        } else {
            upToDateState[value] = {
                selected: true,
            };
        }
        toParentPayload = {
            ...toParentPayload,
            itemsSelectionState: upToDateState,
            selectedOption: {value, title, selected: upToDateState[value].selected}
        };
        return toParentPayload;
    }

    processSingleSelection(selectedOption) {
        selectedOption.selected = true;
        return {
            onClose: this.onClose,
            onOpen: this.onOpen,
            selectedOption,
        };
    }

    onOptionToggle(selectedOption) {
        const {settings, itemsSelectionState} = this.props;
        let toParentPayload;
        if (settings.multiple) {
            toParentPayload = {
                ...this.processMultiSelection(
                    selectedOption, itemsSelectionState
                ),
                settings
            };
            this.props.onSelect(toParentPayload);
        } else {
            toParentPayload = {
                ...this.processSingleSelection(selectedOption),
                settings
            };
            this.props.onSelect(toParentPayload);
            this.onOpenClose(false)();
        }
    }

    onCustomOptionSelect(selectedOption) {
        const toParentPayload = {
            selectedOption,
            onClose: this.onClose,
            onOpen: this.onOpen
        };
        this.props.onCustomOptionSelect(toParentPayload);
    }

    render() {
        const {isOpened, optionsListPosition} = this.state;
        const {
            settings:
                {
                    title,
                    options,
                    customOptions,
                    multiple,
                    icon,
                    iconPosition,
                },
            className,
            selectedOption,
            itemsSelectionState
        } = this.props;

        const displayableTitle = createDisplayableText(title);

        return (
            <div className={classNames("combobox", className)} ref={this.containerRef}>
                {this.props.children}
                <ComboBoxButton
                    ref={this.buttonRef}
                    icon={icon}
                    isOpened={isOpened}
                    title={displayableTitle}
                    iconPosition={iconPosition}
                    onOpenClose={this.onOpenClose}
                >
                    {this.props.childrenToButton}
                </ComboBoxButton>
                {
                    isOpened && options && options.length
                        ? (
                            <ComboBoxOptions
                                optionsListPosition={optionsListPosition}
                                options={options}
                                isOpened={isOpened}
                                multiple={multiple}
                                onClose={this.onClose}
                                onSelect={this.onSelect}
                                customOptions={customOptions}
                                selectedOption={selectedOption}
                                itemsSelectionState={itemsSelectionState}
                                onCustomOptionSelect={this.onCustomOptionSelect}
                            />
                        ) : null
                }
            </div>
        );
    }
}


ComboBox.defaultProps = {
    settings: {
        isOpened: false,
        multiple: false,
        title: "",
        options: [],
        customOptions: []
    },
    optionsPortalPosition: "",
    selectedOption: {},
    itemsSelectionState: {},
    onOpen: () => {
    },
    onClose: () => {
    },
    onSelect: () => {
    },
    onInvalidSelection: () => {
    },
    validateSelection: () => {
        return {error: false, errorMessage: ""};
    },
    onCustomOptionSelect: () => {
    }
};

ComboBox.propTypes = {
    icon: PropTypes.string,
    onOpen: PropTypes.func,
    onClose: PropTypes.func,
    children: PropTypes.any,
    onSelect: PropTypes.func,
    childrenToButton: PropTypes.node,
    settings: PropTypes.object,
    className: PropTypes.string,
    iconPosition: PropTypes.object,
    isMultipleAction: PropTypes.bool,
    selectedOption: PropTypes.object,
    validateSelection: PropTypes.func,
    onInvalidSelection: PropTypes.func,
    onCustomOptionSelect: PropTypes.func,
    itemsSelectionState: PropTypes.object,
    optionsPortalPosition: PropTypes.string
};

export {ComboBox};
