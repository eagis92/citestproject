/**
 *      *** Combo Button Options ***
 * */

import React, {Component} from "react";
import PropTypes from "prop-types";
import {SingleSelectList} from "./SingleSelectList";
import {MultiSelectList} from "./MultiSelectList";
import ReactDOM from "react-dom";
import anime from "animejs";
import classNames from "classnames";


class ComboBoxOptions extends Component {
    constructor(props) {
        super(props);
        this.onSelect = this.onSelect.bind(this);
        this.handleMultiOptions = this.handleMultiOptions.bind(this);
        this.onCustomOptionSelect = this.onCustomOptionSelect.bind(this);
        this.focusOptionsElementNode = this.focusOptionsElementNode.bind(this);
        this.onOutsideClick = this.onOutsideClick.bind(this);

        this.optionsElement = document.createElement("div");
        this.containerRef = React.createRef();
        this.rootElement = document.querySelector("#portal");
    }

    componentDidMount() {
        this.rootElement.appendChild(this.optionsElement);
        document.addEventListener("click", this.onOutsideClick, false);
        this.focusOptionsElementNode();
        this.animateOptions();
    }

    onOutsideClick(e) {
        if (!this.containerRef.current.contains(e.target)) {
            this.props.onClose();
        }
    }

    focusOptionsElementNode() {
        const {multiple} = this.props;
        const optionsNodeClassName = multiple
            ? "combobox__options-list--multiple"
            : "combobox__options-list--single";

        this.optionsNodes = document.getElementsByClassName(optionsNodeClassName);
        this.optionsNodes[0] && this.optionsNodes[0].focus();
    }

    componentWillUnmount() {
        document.removeEventListener("click", this.onOutsideClick, false);
        this.rootElement.removeChild(this.optionsElement);
    }

    handleMultiOptions(e) {
        const value = e.target.value;
        const {options: multiSelectedOptions} = this.props;
        let selectedOption = multiSelectedOptions.filter((option) => option.value === value)[0];
        this.onSelect(selectedOption)();
    }

    onSelect(option) {
        const {onSelect} = this.props;
        return () => {
            onSelect(option);
        };
    }

    onCustomOptionSelect(option) {
        const {onCustomOptionSelect} = this.props;
        return () => {
            onCustomOptionSelect(option);
        };
    }

    animateOptions() {
        anime({
            targets: this.containerRef.current,
            translateY: ["2rem", "0"],
            opacity: [0, 1],
            duration: 400,
            easing: "easeOutCirc",
            delay: 50,
        });
    }

    render() {
        const {
            selectedOption,
            options,
            customOptions,
            multiple,
            onClose,
            itemsSelectionState,
            optionsListPosition,
            className,
        } = this.props;

        return ReactDOM.createPortal(
            <div className={classNames("combobox", className)}>
                <div className="combobox__options" style={optionsListPosition} ref={this.containerRef}>
                    {multiple
                        ? (
                            <MultiSelectList
                                onClose={onClose}
                                metadata={options}
                                onSelect={this.onSelect}
                                itemsSelectionState={itemsSelectionState}
                                handleChange={this.handleMultiOptions}
                            />
                        )
                        : (
                            <SingleSelectList
                                onClose={onClose}
                                options={options}
                                onSelect={this.onSelect}
                                customOptions={customOptions}
                                selectedOption={selectedOption}
                                onCustomOptionSelect={this.onCustomOptionSelect}
                            />
                        )
                    }
                </div>
            </div>,
            this.optionsElement
        );
    }
}

ComboBoxOptions.defaultProps = {
    multiple: false,
    selectedOption: {},
    itemsSelectionState: {},
    options: [],
    customOptions: [],
    onSelect: () => {
    },
    onCustomOptionSelect: () => {
    },
    onClose: () => {
    }
};

ComboBoxOptions.propTypes = {
    optionsListPosition: PropTypes.object,
    onClose: PropTypes.func,
    multiple: PropTypes.bool,
    selectedOption: PropTypes.any,
    customOptions: PropTypes.array,
    onSelect: PropTypes.func.isRequired,
    options: PropTypes.array.isRequired,
    onCustomOptionSelect: PropTypes.func,
    itemsSelectionState: PropTypes.object,
    className: PropTypes.any,
};

export default ComboBoxOptions;
