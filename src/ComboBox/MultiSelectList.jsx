/**
 *      *** Combo Button Multiple Selection Options ***
 * */

import React from "react";
import PropTypes from "prop-types";
import {ENTER, ESCAPE, KEY_DOWN, KEY_PRESS_CODES, KEY_UP, TAB} from "../helpers";
import classNames from "classnames";

class MultiSelectList extends React.Component {
    constructor(props) {
        super(props);
        this.onKeyDown = this.onKeyDown.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
        this.onKeyUpDown = this.onKeyUpDown.bind(this);
        this.onNavigateUp = this.onNavigateUp.bind(this);
        this.onNavigateDown = this.onNavigateDown.bind(this);
        this.getOnHighlightedOptionIndex = this.getOnHighlightedOptionIndex.bind(this);

        this.highlightedOptionIndex = null;
        this.state = {
            highlightedOption: {}
        };
    }

    onKeyPress(event) {
        const keyCode = event.keyCode || event.which;
        if (keyCode === KEY_PRESS_CODES[ENTER]) {
            const {highlightedOption} = this.state;
            const {
                onSelect
            } = this.props;

            onSelect(highlightedOption)();
        }
    }

    onKeyDown(event) {
        const keyCode = event.keyCode || event.which;
        if (keyCode === KEY_PRESS_CODES[TAB]) {
            this.props.onClose();
        }
    }

    onKeyUpDown(e) {
        if (e.keyCode === KEY_PRESS_CODES[KEY_DOWN]) {
            this.onNavigateDown();
        } else if (e.keyCode === KEY_PRESS_CODES[KEY_UP]) {
            this.onNavigateUp();
        } else if (e.keyCode === KEY_PRESS_CODES[ESCAPE]) {
            this.props.onClose();
        }
    }

    getOnHighlightedOptionIndex(eventType = "down", allOptions) {
        if (this.highlightedOptionIndex === null) {
            if (eventType === "down") {
                return this.highlightedOptionIndex = 0;
            } else {
                return this.highlightedOptionIndex = (allOptions.length - 1);
            }
        }
        if (eventType === "down") {
            if (this.highlightedOptionIndex === allOptions.length - 1) {
                this.highlightedOptionIndex = 0;
                return this.highlightedOptionIndex;
            }
            this.highlightedOptionIndex += 1;
            return this.highlightedOptionIndex;
        } else {
            if (this.highlightedOptionIndex === 0) {
                this.highlightedOptionIndex = allOptions.length - 1;
                return this.highlightedOptionIndex;
            }
            this.highlightedOptionIndex -= 1;
            return this.highlightedOptionIndex;
        }
    }

    onNavigateUp() {
        const {metadata} = this.props;

        const highlightedOptionIndex = this.getOnHighlightedOptionIndex(
            "",
            metadata
        );
        this.setState({
            highlightedOption: metadata[highlightedOptionIndex]
        });
    }

    onNavigateDown() {
        const {metadata} = this.props;

        const highlightedOptionIndex = this.getOnHighlightedOptionIndex(
            "down",
            metadata
        );
        this.setState({
            highlightedOption: metadata[highlightedOptionIndex]
        });
    }

    render() {
        const {metadata, itemsSelectionState, handleChange} = this.props;
        const {highlightedOption: {value}} = this.state;
        return (
            <ul tabIndex="0"
                onKeyPress={this.onKeyPress}
                onKeyDown={this.onKeyDown}
                onKeyUp={this.onKeyUpDown}
                className="combobox__options-list combobox__options-list--multiple">
                {
                    metadata.map((option) => {
                        const highlighted = value === option.value ? 'combobox__option-item--highlighted' : '';
                        const hasCount = option.hasOwnProperty("count");
                        return (
                            <li key={option.value}
                                className={classNames(
                                    highlighted,
                                    option.style,
                                    "combobox__option-item"
                                )}
                            >
                                <label
                                    className={classNames("control checkbox combobox__option-item-checkbox",
                                        (itemsSelectionState[option.value] && itemsSelectionState[option.value].selected ? "checked" : ""))}>
                                    <input
                                        type="checkbox"
                                        name={option.label}
                                        value={option.value}
                                        checked={(!!itemsSelectionState[option.value] && itemsSelectionState[option.value].selected)}
                                        onChange={handleChange}
                                    />

                                    <span className="control__indicator"/>

                                    {option.title}

                                    <span className="combobox__option-item-count">
                                        {hasCount && option.count}
                                    </span>
                                </label>
                            </li>
                        );
                    })

                }
            </ul>
        );
    }
}

MultiSelectList.defaultProps = {
    itemsSelectionState: {},
    metadata: [],
    onClose: () => {
    },
    handleChange: () => {
    },
};

MultiSelectList.propTypes = {
    onClose: PropTypes.func,
    metadata: PropTypes.any,
    handleChange: PropTypes.func,
    onSelect: PropTypes.func.isRequired,
    itemsSelectionState: PropTypes.object,
};

export {MultiSelectList};
