/**
 *      *** Combo Button Single Selection Options ***
 * */

import React, {Component} from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {
    KEY_DOWN,
    KEY_UP,
    ENTER,
    ESCAPE,
    TAB,
    KEY_PRESS_CODES,
    getHighlightedOptionIndex
} from "../helpers.jsx";
import {createDisplayableText} from "../helpers";

class SingleSelectList extends Component {
    constructor(props) {
        super(props);
        this.optionsRef = React.createRef();
        this.onKeyUpDown = this.onKeyUpDown.bind(this);
        this.onNavigateUp = this.onNavigateUp.bind(this);
        this.onNavigateDown = this.onNavigateDown.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);
        this.onCustomOptionSelect = this.onCustomOptionSelect.bind(this);
        this.getOnHighlightedOptionIndex = this.getOnHighlightedOptionIndex.bind(this);
        const {options, customOptions, selectedOption} = props;
        const allOptions = options.concat(customOptions);
        this.highlightedOptionIndex = getHighlightedOptionIndex(selectedOption, allOptions);
        this.state = {
            highlightedOption: selectedOption
        };
    }

    componentDidMount() {
        if (this.optionsRef.current) {
            this.optionsRef.current.focus();
        }
    }

    onKeyDown(event) {
        const keyCode = event.keyCode || event.which;
        if (keyCode === KEY_PRESS_CODES[TAB]) {
            this.props.onClose();
        }
    }

    onKeyPress(event) {
        const keyCode = event.keyCode || event.which;
        if (keyCode === KEY_PRESS_CODES[ENTER]) {
            const {highlightedOption} = this.state;
            const {
                options,
                onSelect,
                onCustomOptionSelect
            } = this.props;

            const optionsMaxIndex = options.length - 1;
            if (this.highlightedOptionIndex !== null) {
                if (this.highlightedOptionIndex <= optionsMaxIndex) {
                    onSelect(highlightedOption)();
                } else {
                    onCustomOptionSelect(highlightedOption)();
                }
            }
        }
    }

    onKeyUpDown(e) {
        if (e.keyCode === KEY_PRESS_CODES[KEY_DOWN]) {
            this.onNavigateDown();
        } else if (e.keyCode === KEY_PRESS_CODES[KEY_UP]) {
            this.onNavigateUp();
        } else if (e.keyCode === KEY_PRESS_CODES[ESCAPE]) {
            this.props.onClose();
        }
    }

    getOnHighlightedOptionIndex(eventType = "down", allOptions) {
        if (this.highlightedOptionIndex === null) {
            if (eventType === "down") {
                return this.highlightedOptionIndex = 0;
            } else {
                return this.highlightedOptionIndex = (allOptions.length - 1);
            }
        }
        if (eventType === "down") {
            if (this.highlightedOptionIndex === allOptions.length - 1) {
                this.highlightedOptionIndex = 0;
                return this.highlightedOptionIndex;
            }
            this.highlightedOptionIndex += 1;
            return this.highlightedOptionIndex;
        } else {
            if (this.highlightedOptionIndex === 0) {
                this.highlightedOptionIndex = allOptions.length - 1;
                return this.highlightedOptionIndex;
            }
            this.highlightedOptionIndex -= 1;
            return this.highlightedOptionIndex;
        }
    }

    onNavigateUp() {
        const {options, customOptions} = this.props;
        const allOptions = options.concat(customOptions);

        const highlightedOptionIndex = this.getOnHighlightedOptionIndex(
            "",
            allOptions
        );
        this.setState({
            highlightedOption: allOptions[highlightedOptionIndex]
        });
    }

    onNavigateDown() {
        const {options, customOptions} = this.props;
        const allOptions = options.concat(customOptions);

        const highlightedOptionIndex = this.getOnHighlightedOptionIndex(
            "down",
            allOptions
        );
        this.setState({
            highlightedOption: allOptions[highlightedOptionIndex]
        });
    }

    onCustomOptionSelect(selectedCustomOption) {
        return () => {
            this.setState({
                highlightedOption: selectedCustomOption
            }, this.props.onCustomOptionSelect(selectedCustomOption));

            const {options, customOptions} = this.props;
            const allOptions = options.concat(customOptions);
            this.highlightedOptionIndex = getHighlightedOptionIndex(selectedCustomOption, allOptions);
        };
    }

    render() {
        const {options, onSelect, customOptions} = this.props;
        const {highlightedOption} = this.state;
        return (
            <ul ref={this.optionsRef}
                tabIndex="0"
                onKeyPress={this.onKeyPress}
                onKeyDown={this.onKeyDown}
                onKeyUp={this.onKeyUpDown}
                className="combobox__options-list combobox__options-list--single">
                {
                    options.map((singleOption) => {
                        const isSelectedFilter = highlightedOption.value === singleOption.value;
                        const displayableTitle = createDisplayableText(singleOption.title);
                        const highlighted = isSelectedFilter ? 'combobox__option-item--highlighted' : '';
                        const hasCount = singleOption.hasOwnProperty("count");
                        return (
                            <li key={singleOption.value}
                                className={classNames(
                                    highlighted,
                                    singleOption.style,
                                    "combobox__option-item"
                                )}
                            >
                                <button
                                    type="button"
                                    className="btn btn-clear dark combobox__option-item-button"
                                    onClick={onSelect(singleOption)}>

                                    <label>
                                        {displayableTitle}
                                    </label>
                                    <span className="combobox__option-item-count">
                                    {hasCount && singleOption.count}
                                </span>
                                </button>
                            </li>
                        );
                    })
                }

                {
                    customOptions.length ?
                        <React.Fragment>
                            <div className="combobox__options-list-divider"/>
                            {
                                customOptions.map((singleOption) => {
                                    const highlighted = highlightedOption.value === singleOption.value
                                        ? "combobox__option-item--highlighted"
                                        : "";
                                    return (
                                        <li key={singleOption.value}
                                            className={classNames(highlighted, "combobox__option-item")}>
                                            <button
                                                type="button"
                                                className="btn btn-clear dark combobox__option-item-button"
                                                onClick={this.onCustomOptionSelect(singleOption)}
                                            >
                                                <label>
                                                    {singleOption.label}
                                                </label>
                                            </button>
                                        </li>
                                    );
                                })
                            }
                        </React.Fragment>
                        :
                        null
                }
            </ul>
        );
    }
}

SingleSelectList.defaultProps = {
    options: [],
    customOptions: [],
    onSelect: () => {
    },
};

SingleSelectList.propTypes = {
    selectedOption: PropTypes.object,
    options: PropTypes.array.isRequired,
    onSelect: PropTypes.func.isRequired,
    customOptions: PropTypes.array,
    onCustomOptionSelect: PropTypes.func,
    onClose: PropTypes.func,
};

export {SingleSelectList};
