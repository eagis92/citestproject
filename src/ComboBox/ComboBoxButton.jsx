/**
 *      *** Combo Button Header Component ***
 * */

import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button} from "../ui-components/Button";
import {KEY_UP, KEY_DOWN, ESCAPE, KEY_PRESS_CODES} from "../helpers.jsx";
import classNames from "classnames";

class ComboBoxButton extends Component {
    constructor(props) {
        super(props);
        this.onKeyUpDown = this.onKeyUpDown.bind(this);
    }

    onKeyUpDown(e) {
        const keyBoardCode = e.keyCode || e.which;

        if (keyBoardCode === KEY_PRESS_CODES[KEY_UP]) {
            this.props.onOpenClose(false)(e);
        } else if (keyBoardCode === KEY_PRESS_CODES[KEY_DOWN]) {
            this.props.onOpenClose(true)(e);
        } else if (e.keyCode === KEY_PRESS_CODES[ESCAPE]) {
            this.props.onOpenClose(false)();
        }
    }

    render() {
        const {
            isOpened,
            title,
            onOpenClose,
            onFocusButton,
            children,
            className,
        } = this.props;

        return(
            <Button
                icon="arrow-down-12"
                label={title}
                onFocus={onFocusButton}
                onKeyUp={this.onKeyUpDown}
                iconPosition="right"
                onClick={onOpenClose(!isOpened)}
                className={classNames("combobox__toggle-button", className, {"is-open": isOpened})}
            >
                {children}
            </Button>
        );
    }
}


ComboBoxButton.defaultProps= {
    onFocusButton: () => {},
    onBlur: () => {},
    onOpenClose: () => {},
    icon: "",
    iconPosition: ""
};

ComboBoxButton.propTypes = {
    onBlur: PropTypes.func,
    icon: PropTypes.string,
    title: PropTypes.string,
    children: PropTypes.node,
    isOpened: PropTypes.bool,
    onOpenClose: PropTypes.func,
    className: PropTypes.string,
    onFocusButton: PropTypes.func,
    iconPosition: PropTypes.string
};

export default ComboBoxButton;
