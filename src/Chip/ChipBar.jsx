/**
 *      *** Chip Strip Component ***
 * */


import React, {Component} from "react";
import PropTypes from "prop-types";
import {Chip} from "./Chip";
import {Button} from "../ui-components/Button";


class ChipBar extends Component {
    constructor(props) {
        super(props);
        this.onChipRemove = this.onChipRemove.bind(this);
        this.onRemoveAll = this.onRemoveAll.bind(this);
    }

    onChipRemove(chipMeta) {
        const {onChipRemove} = this.props;
        onChipRemove(chipMeta);
    }

    onRemoveAll() {
        const {onRemoveAll} = this.props;
        onRemoveAll();
    }

    render() {
        const {chips, clearAllText, icon} = this.props;
        return (
            <div className="chip-bar">
                {
                    chips.map(
                        (chip) => (
                            <Chip
                                {...chip}
                                icon={icon}
                                key={`${chip.value + (chip.name || "")}`}
                                onRemove={this.onChipRemove}
                            />
                        )
                    )
                }
                <Button
                    className="chip chip__clear-all"
                    styleType="clear dark"
                    label={clearAllText}
                    icon={icon}
                    onClick={this.onRemoveAll}
                />
            </div>
        );
    }
}


ChipBar.defaultProps = {
    chips: [],
    icon: "close",
    clearAllText: "Clear All",
    onChipRemove: () => {
    },
    onRemoveAll: () => {
    }
};

ChipBar.propTypes = {
    chips: PropTypes.array,
    clearAllText: PropTypes.string,
    icon: PropTypes.string,
    onRemoveAll: PropTypes.func,
    onChipRemove: PropTypes.func
};

export {ChipBar};
