/**
 *      *** Chip Component ***
 * */

import React, {Component} from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {Button} from "../ui-components/Button";


class Chip extends Component {
    constructor(props) {
        super(props);
        this.remove = this.remove.bind(this);
    }

    remove(chipMeta) {
        return () => {
            const {onInvalidRemoval, onRemove} = this.props;
            const validationOutcome = this.props.validateRemove(chipMeta);
            if (validationOutcome.error) {
                onInvalidRemoval(chipMeta, validationOutcome);
            } else {
                onRemove(chipMeta);
            }
        };
    }

    render() {
        const {
            title,
            label,
            name,
            value,
            type,
            icon,
            labelStyle
        } = this.props;

        return (
            <Button
                className="chip"
                styleType="primary"
                icon={icon}
                iconPosition="right"
                onClick={this.remove({name, value, type})}>

                <span className={classNames("chip__key", labelStyle)}>{label}:</span>
                <span className="chip__value">{title}</span>
            </Button>
        );
    }
}


Chip.defaultProps = {
    title: "",
    type: "",
    label: "",
    labelStyle: "",
    icon: "close",
    onRemove: () => {
    },
    validateRemove: () => {
        return {error: false, errorMessage: ""};
    },
    onInvalidRemoval: () => {
    }
};

Chip.propTypes = {
    value: PropTypes.any,
    icon: PropTypes.string,
    type: PropTypes.string,
    name: PropTypes.string,
    title: PropTypes.string,
    label: PropTypes.string,
    onRemove: PropTypes.func,
    settings: PropTypes.object,
    labelStyle: PropTypes.string,
    validateRemove: PropTypes.func,
    onInvalidRemoval: PropTypes.func,
};

export {Chip};
