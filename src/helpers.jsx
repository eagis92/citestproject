export const TAB = "TAB";
export const ENTER = "ENTER";
export const ESCAPE = "ESCAPE";
export const KEY_UP = "KEY_UP";
export const KEY_DOWN = "KEY_DOWN";

export const MAX_ALLOWED_FILTER_NAME_LENGTH = 256;

export const KEY_PRESS_CODES = {
    [TAB]: 9,
    [ENTER]: 13,
    [ESCAPE]: 27,
    [KEY_UP]: 38,
    [KEY_DOWN]: 40
};

export function isEmptyObject(inputObject) {
    return !!(
        inputObject &&
        !Array.isArray(inputObject) &&
        Object.keys(inputObject).length === 0
    );
}

export function getInitialSelectedOption(
    {settings: {options, multiple, customOptions}}
) {
    if (!multiple && options && options.length) {
        let allOptions;
        if (customOptions && customOptions.length) {
            allOptions = options.concat(customOptions);
        }
        return allOptions.filter(option => option.selected)[0];
    }
    return {};
}

export function getHighlightedOptionIndex(selectedOption, allOptions) {
    let highlightedOptionIndex = null;
    allOptions.forEach((option, index) => {
        if (option.value === selectedOption.value) {
            highlightedOptionIndex = index;
        }
    });
    return highlightedOptionIndex;
}

export function getComboOptionPosition(isOpened, browserEvent, optionsPortalPosition) {
    if (isOpened) {
        const clientRect = browserEvent.currentTarget.getBoundingClientRect();
        let top = clientRect.top + clientRect.height;
        if (optionsPortalPosition === "left") {
            let left = clientRect.left;
            return {
                top,
                left,
            };
        } else {
            let right = -clientRect.right;
            return {
                top,
                right,
            };
        }
    }
}

export function generateDefaultTitle(options = []) {
    let defaultNamePattern = "filter[{0}]";
    let optionsHavingFilterInName = options.filter(({title}) => title.split("[")[0] === "filter");

    let sortedFilterNames = options.reduce(
        (collectedNames, {title}) => {
            if (title.split("[")[0] === "filter") {
                const numberPartWithBracket = title.split("filter")[1];
                const numberPartName = Number(numberPartWithBracket.slice(1, numberPartWithBracket.length -1));
                if (numberPartName) {
                    collectedNames.push(numberPartName);
                }
            }
            return collectedNames;
        }, []);
    sortedFilterNames.sort((a, b) => a - b);
    return sortedFilterNames[sortedFilterNames.length - 1]
        ? String.format(defaultNamePattern, ++sortedFilterNames[sortedFilterNames.length - 1])
        : String.format(defaultNamePattern, ++optionsHavingFilterInName.length);
}

export function createDisplayableText (titleText = "") {
    const THRESHOLD_TO_SHOW_DOTS = 100;
    return titleText && titleText.length > THRESHOLD_TO_SHOW_DOTS ? titleText.substring(0, 25) + "..." : titleText;
}

export const getSavedFilterSettingByTitle = (filterName, sectionFilters) => {
    let settings = null;
    sectionFilters.forEach((filter) => {
        if (filter.title === filterName) {
            settings = filter;
        }
    });
    return settings;
};
