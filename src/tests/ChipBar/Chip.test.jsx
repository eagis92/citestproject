import React from "react";
import {mount, configure} from 'enzyme';
import {ChipBar} from '../../Chip/ChipBar';
import {Chip} from '../../Chip/Chip';
import Adapter from 'enzyme-adapter-react-16';
import renderer from "react-test-renderer";

configure({adapter: new Adapter()});

describe('Snapshot test', () => {

    it('ChipBar snapshot', () => {
        const chips = [
            {
                label: "Last 24 hours",
                title: "Date/Time",
                titleStyle: "",
                value: 0
            },
            {
                label: "This Month",
                title: "Date/Time",
                titleStyle: "",
                value: 1
            },
        ];
        const snapshotTree = renderer.create(<ChipBar chips={chips} icon={""}/>).toJSON();
        expect(snapshotTree).toMatchSnapshot();
    });

    it('Chip snapshot', () => {
        const settings = {
            label: "Last 24 hours",
            title: "Date/Time",
            titleStyle: "",
            value: 0
        };
        const snapshotTree = renderer.create(<Chip settings={settings} icon={""}/>).toJSON();
        expect(snapshotTree).toMatchSnapshot();
    });
});

describe('Default props are set properly', () => {

    it('ChipBar rendered without errors', () => {
        mount(<ChipBar icon={""}/>);
    });

    it('Chip rendered without errors', () => {
        mount(<Chip icon={""}/>);
    });
});

describe('ChipBar shows chips properly', () => {

    it('ChipBar must have 2 chips', () => {
        const chips = [
            {
                label: "Last 24 hours",
                title: "Date/Time",
                titleStyle: "",
                value: 0
            },
            {
                label: "This Month",
                title: "Date/Time",
                titleStyle: "",
                value: 1
            },
        ];
        const MountedChipBar = mount(<ChipBar chips={chips} icon={""}/>);
        expect(MountedChipBar.find("Chip")).toHaveLength(chips.length);
    });
});

describe('Chip properties are set properly', () => {

    it('Chip label matches with expectations', () => {
        const settings = {
            label: "Last 24 hours",
            title: "Date/Time",
            titleStyle: "",
            value: 0
        };

        const MountedChip = mount(<Chip {...settings} icon={""}/>);
        expect(MountedChip.find("span").first().text()).toEqual('Last 24 hours:');
    });

    it('Chip title matches with expectations', () => {
        const settings = {
            label: "Last 24 hours",
            title: "Date/Time",
            titleStyle: "",
            value: 0
        };

        const MountedChip = mount(<Chip {...settings} icon={""}/>);
        expect(MountedChip.find("span").last().text()).toEqual(settings.title);
    });
});

describe('ChipBar onRemoveAll functionality', () => {

    it('Being triggered on click', () => {
        const chips = [
            {
                label: "Last 24 hours",
                title: "Date/Time",
                titleStyle: "",
                value: 0
            },
            {
                label: "This Month",
                title: "Date/Time",
                titleStyle: "",
                value: 1
            },
        ];
        const onRemoveAll = jest.fn();
        const componentTree = mount(<ChipBar chips={chips} onRemoveAll={onRemoveAll} icon={""}/>);
        const onRemoveAllButton = componentTree.find("button").last();
        onRemoveAllButton.simulate("click");
        expect(onRemoveAll).toHaveBeenCalledTimes(1);
    });
});

describe('Chip onClear functionality', () => {
    it('Being triggered on click', () => {
        const settings = {
            label: "Last 24 hours",
            title: "Date/Time",
            titleStyle: "",
            value: 0
        };
        const onRemove = jest.fn();
        const MountedChip = mount(<Chip {...settings}
                                        icon={""}
                                        onRemove = {onRemove}/>);


        const onRemoveButton = MountedChip.find("button");
        onRemoveButton.simulate("click");
        expect(onRemove).toHaveBeenCalledTimes(1);
    });
});

describe('Chip receives custom style', () => {
    it('styleName matches with expectations', () => {
        const settings = {
            label: "Last 24 hours",
            title: "Date/Time",
            labelStyle: "custom-style-name",
            value: 0
        };
        const MountedChip = mount(<Chip {...settings} icon={""}/>);
        expect(MountedChip.find(".custom-style-name").exists()).toEqual(true);
    });
});
