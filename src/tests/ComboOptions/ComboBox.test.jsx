import React from "react";
import ReactDOM from "react-dom";
import {ComboBox} from '../../ComboBox/ComboBox.jsx';
import {configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TestRenderer from "react-test-renderer";

configure({adapter: new Adapter()});

describe('Snapshot test', () => {
    it('ComboBox single selection snapshot', () => {
        const settings = {
            id: "1d926cc1-68ca-45cf-9e8a-6e9da419b709",
            icon: "",
            isOpened: false,
            multiple: false,
            promptText: "Date/Time",
            options: [
                {
                    label: "24 Hours",
                    value: "1",
                    count: 2,
                    style: "someClassName"
                }
            ],
            customOptions: [
                {
                    label: "Custom Date/Time",
                    value: "111",
                    count: 111
                }
            ]
        };

        const snapshotTree = TestRenderer.create(<ComboBox settings={settings}/>).toJSON();
        expect(snapshotTree).toMatchSnapshot();
    });

    it('ComboBox multiple selection snapshot', () => {
        const settings = {
            id: "1d926cc1-68ca-45cf-9e8a-6e9da419b709",
            icon: "",
            isOpened: false,
            multiple: true,
            promptText: "Date/Time",
            options: [
                {
                    label: "24 Hours",
                    value: "1",
                    count: 2,
                    style: "someClassName"
                }
            ],
            customOptions: [
                {
                    label: "Custom Date/Time",
                    value: "111",
                    count: 111
                }
            ]
        };

        const snapshotTree = TestRenderer.create(<ComboBox settings={settings}/>).toJSON();
        expect(snapshotTree).toMatchSnapshot();
    });
});

describe('ComboBox single selection component', () => {
    beforeAll(() => {
        ReactDOM.createPortal = jest.fn((element) => {
            return element
        })
    });

    afterEach(() => {
        ReactDOM.createPortal.mockClear()
    });

    it("Default props must be specified properly", () => {

        const modalRoot = global.document.createElement('div');
        modalRoot.setAttribute('id', 'portal');
        const body = global.document.querySelector('body');
        body.appendChild(modalRoot);

        TestRenderer.create(<ComboBox/>);
    });

});

describe('ComboBox multiple selection component', () => {
    beforeAll(() => {
        ReactDOM.createPortal = jest.fn((element) => {
            return element
        })
    });

    afterEach(() => {
        ReactDOM.createPortal.mockClear()
    });


    it("Default props must be specified properly", () => {
        const modalRoot = global.document.createElement('div');
        modalRoot.setAttribute('id', 'portal');
        const body = global.document.querySelector('body');
        body.appendChild(modalRoot);

        const settings = {
            multiple: true
        };
        TestRenderer.create(<ComboBox settings={settings}/>);
    });

    it("Options initially selected/deselected", () => {

        const modalRoot = global.document.createElement('div');
        modalRoot.setAttribute('id', 'portal');
        const body = global.document.querySelector('body');
        body.appendChild(modalRoot);

        const settings = {
            id: "1d926cc1-68ca-45cf-9e8a-6e9da419b709",
            icon: "",
            isOpened: true,
            multiple: true,
            promptText: "Date/Time",
            options: [{
                label: "24 Hours",
                value: "1",
                count: 2,
                selected: true
            }, {
                label: "Last Month",
                value: "2",
                count: 6
            }]
        };

        const component = TestRenderer.create(
            <ComboBox
                settings={settings}
                itemsSelectionState={
                    {
                        "1": {selected: true},
                        "2": {selected: false}
                    }
                }
            />);

        expect(
            component.root
                .findAllByType("input")[0]
                .props
                .checked
        ).toEqual(true);

        expect(
            component.root
                .findAllByType("input")[1]
                .props
                .checked
        ).toEqual(false);

    });

    it("ComboBox Options should be in closed state", () => {
        const settings = {
            id: "1d926cc1-68ca-45cf-9e8a-6e9da419b709",
            icon: "",
            isOpened: false,
            multiple: false,
            promptText: "Date/Time",
            options: []
        };

        const component = TestRenderer.create(
            <ComboBox
                settings={settings}
            />);
        expect(
            component.getInstance().state.isOpened
        ).toEqual(false);
    });

    it("onOpen / onClose should be called", () => {
        const settings = {
            id: "1d926cc1-68ca-45cf-9e8a-6e9da419b709",
            icon: "",
            isOpened: false,
            multiple: false,
            promptText: "Date/Time",
        };
        const mockedOnOpen = jest.fn();
        const mockedOnClose = jest.fn();

        const componentTree = TestRenderer.create(
            <ComboBox
                settings={settings}
                onOpen={mockedOnOpen}
                onClose={mockedOnClose}
            />);

        componentTree.getInstance().props.onOpen();
        componentTree.getInstance().props.onClose();

        expect(mockedOnOpen).toHaveBeenCalledTimes(1);
        expect(mockedOnClose).toHaveBeenCalledTimes(1);
    });
});