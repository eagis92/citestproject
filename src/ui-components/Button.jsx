/*
 *  Button
 *
 *
 */

import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
// import SVGIcon from "./SVGIcon";

class Button extends Component {
    constructor(props) {
        super(props);

        /*
        this.splashEl = React.createRef();

        this.state = {
            x: 0,
            y: 0
        };

        this.addSplashEl = this.addSplashEl.bind(this);
        this.animateSplashEl = this.animateSplashEl.bind(this);
        this.setClickCoordinates = this.setClickCoordinates.bind(this);
        */
        this.state = {
            isActive: false
        };
        this.handleClick = this.handleClick.bind(this);
        this.handleOnKeyUp = this.handleOnKeyUp.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onFocus = this.onFocus.bind(this);
    }

    onBlur() {
        const {onBlur} = this.props;
        const { isActive } = this.state;
        isActive && this.setState({isActive: false});
        if (onBlur) {
            onBlur();
        }
    }

    onFocus() {
        const {onFocus} = this.props;
        if (onFocus) {
            onFocus();
        }
    }

    handleClick(e) {
        const { onClick } = this.props;
        const { isActive } = this.state;
        !isActive && this.setState({isActive: true});
        /*
        this.setClickCoordinates(e);
        this.addSplashEl();
        this.animateSplashEl();
        */

        // callback
        if (onClick) {
            onClick(e);
        }
    }

    handleOnKeyUp(e) {
        const {onKeyUp} = this.props;
        if (onKeyUp) {
            onKeyUp(e);
        }
    }

    /*
    setClickCoordinates(e) {
        this.setState({
            x: e.nativeEvent.offsetX,
            y: e.nativeEvent.offsetY
        });
    }

    addSplashEl() {
        this.el = document.createElement("div");
        this.splashEl.append(this.el);
    }

    animateSplashEl() {
        //this.el.animate({
        //    ...
        //});
    }
    */

    render() {
        const { label,
                mobileLabel,
                className,
                styleType,
                type,
                viewMobile,
                viewTablet,
                viewDesktop,
                disabled,
                tabIndex,
                children } = this.props;

        const { isActive } = this.state;

        const btnStyleType = "btn-" + styleType;

        return (
            <button
                tabIndex={tabIndex}
                type={type}
                onBlur={this.onBlur}
                onFocus={this.onFocus}
                className={classNames( "btn", btnStyleType, className, {
                    "is-active": isActive,
                    "mobile-hidden": !viewMobile,
                    "portable-hidden": !viewTablet,
                    "desktop-hidden": !viewDesktop,
                    "disabled": disabled,
                })}
                onClick={this.handleClick}
                onKeyUp={this.handleOnKeyUp}>

                <div className="btn__content-wrapper">

                    {label &&
                        <label data-mobile-shown={mobileLabel}>{label}</label>
                    }

                    {children}


                </div>

                {/*<div className="btn__splash-wrapper" ref={this.splashEl}></div>*/}
            </button>
        );
    }
}

Button.defaultProps = {
    type: "button",
    tabIndex: 0,
    children: null,
    styleType: "default",
    iconPosition: "left",
    mobileLabel: "true",
    viewMobile: true,
    viewTablet: true,
    viewDesktop: true,
};

Button.propTypes = {
    /** Set icon for the input */
    icon: PropTypes.string,
    /** Set the label for the input */
    label: PropTypes.string,
    /*Set the children for the input*/
    children: PropTypes.node,
    /** Set the type of the input */
    type: PropTypes.string,
    /** Set the style type of the input */
    styleType: PropTypes.string,
    /** Set icon position for the input */
    iconPosition: PropTypes.string,
    /** Set the handler for when a user clicks the input */
    onClick: PropTypes.func,
    /** Set the tab index*/
    tabIndex: PropTypes.number,
    /** Set the handler for keyboard */
    onKeyUp: PropTypes.func,
    /** Set the handler for onBlur */
    onBlur: PropTypes.func,
    /** Set the handler for onFocus */
    onFocus: PropTypes.func,
    /** Set the mobile label for the input */
    mobileLabel: PropTypes.string,
    /** Pass additional class names for the input */
    className: PropTypes.string,
    /** Set the whether the input should be visible on mobile*/
    viewMobile: PropTypes.bool,
    /** Set the whether the input should be visible on tablets*/
    viewTablet: PropTypes.bool,
    /** Set the whether the input should be visible on desktop*/
    viewDesktop: PropTypes.bool,
    /** Set whether the input should be disabled */
    disabled: PropTypes.bool,
};

export {Button};
